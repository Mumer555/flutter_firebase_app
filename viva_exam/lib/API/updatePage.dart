
import 'package:viva_exam/API/cruds.dart';
import 'package:flutter/material.dart';

class ApiUpdatePage extends StatelessWidget {

  Crud cruds = Crud();
  final titleController = TextEditingController();
  final descriptionController = TextEditingController();
  final statusController = TextEditingController();


  @override
  Widget build(BuildContext context) {

  var data = ModalRoute.of(context)?.settings.arguments as List<dynamic>;
  if(data[1] != "")
  {
    titleController.text = data[1];
  }
  if(data[2] != "")
  {
    descriptionController.text = data[2];
  }
  if(data[3] != "")
  {
    statusController.text = data[3];
  }

    return Scaffold(
      appBar: AppBar(
        title: Text("Update Services"),
        centerTitle: true,
        backgroundColor: Colors.orangeAccent,
      ),
      body: Container(
        padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            TextField(
              controller: titleController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'UserId',
              ),
            ),
            SizedBox(height: 8.0,),
            TextField(
              controller: descriptionController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Title',
              ),
            ),
            SizedBox(height: 8.0,),
            TextField(
              controller: statusController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Completion Status',
              ),
            ),
            SizedBox(height: 8.0,),
            ElevatedButton(
              onPressed: () async {
                bool response = await cruds.updateServices(data[0],titleController.text, descriptionController.text, statusController.text);
                if(response)
                {
                  Navigator.pushNamed(context, "/ApiHome");
                }
                else{
                  print("could not update service");
                }
              }, 
              child: Text("Update",style: TextStyle(fontWeight: FontWeight.bold),),
            )
          ],
        ),
      ),
    );
  }
}