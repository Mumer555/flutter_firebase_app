
class Service {
  String id;
  String uid;
  String title;
  String completed;

  Service({required this.id , required this.uid , required this.title, required this.completed});
}