import 'package:viva_exam/API/cruds.dart';
import 'package:flutter/material.dart';

class ApiCreatePage extends StatelessWidget {


  Crud cruds = Crud();
  final titleController = TextEditingController();
  final descriptionController = TextEditingController();
  final statusController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Create Services"),
        centerTitle: true,
        backgroundColor: Colors.orangeAccent,
      ),
      body: Container(
        padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            TextField(
              controller: titleController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'User ID',
              ),
            ),
            SizedBox(height: 8.0,),
            TextField(
              controller: descriptionController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Title',
              ),
            ),
            SizedBox(height: 8.0,),
            TextField(
              controller: statusController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Completion Status',
              ),
            ),
            SizedBox(height: 8.0,),
            ElevatedButton(
              onPressed: () async {
                bool response = await cruds.createServices(int.parse(titleController.text), descriptionController.text, statusController.text);
                if(response)
                {
                  Navigator.pushNamed(context, "/ApiHome");
                }
                else{
                  print("could not create service");
                }
              }, 
              child: Text("Submit",style: TextStyle(fontWeight: FontWeight.bold),),
            )
          ],
        ),
      ),
    );
  }
}
