
import 'package:viva_exam/API/Service.dart';
import 'package:viva_exam/API/cruds.dart';
import 'package:flutter/material.dart';

class ApiHome extends StatefulWidget {
  const ApiHome({super.key});

  @override
  State<ApiHome> createState() => _ApiHomeState();
}

class _ApiHomeState extends State<ApiHome> {
  
  Crud cruds = Crud();
  List<Service> services = [];
  
  getData() async {
    services = await cruds.getData();
  }

  Widget getCardTemplate(element) {

    return Dismissible(
      key: Key(element.id),
      direction: DismissDirection.endToStart,
      onDismissed: (direction) async {
        bool response = await cruds.deleteServices(int.parse(element.id));
        if(response == false){
          print("can not delete service");
        }
      },
      child: InkWell(
        onTap: (){
          Navigator.pushNamed(context, "/Apiupdate" , arguments: [element.id , element.uid , element.title ,element.completed]);
        },
        child: Card(
          color: Colors.grey[100],
          child: Container(
            padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0.0),
            margin: EdgeInsets.all(5.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text(element.uid,style: TextStyle(fontWeight: FontWeight.bold),),
                Text(element.title),
                Text(element.completed,style: TextStyle(fontWeight: FontWeight.bold),),
              ],
            ),
          ),
        ),
      ),
    );

  }
  
  @override
  void initState() {
    super.initState();
    getData();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Crud on RestApi"),
        centerTitle: true,
        backgroundColor: Colors.orangeAccent,
      ),
      body: FutureBuilder(
        future: getData(),
        builder: (context , snapshot) {
          if(snapshot.connectionState == ConnectionState.done)
          {
            return Scrollbar(
              child: ListView.builder(
                itemCount: 1,
                itemBuilder: (context , index) {
                  return Column(
                    children: services.map((element) => getCardTemplate(element)).toList(),
                  );
                }
              ),
            );
          }
          else
          {
            return CircularProgressIndicator();
          }
        },

      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, "/Apicreate");
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.orangeAccent,
      ),
    );
  }
}