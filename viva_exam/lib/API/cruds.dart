import 'dart:convert';
import 'dart:developer';

import 'package:viva_exam/API/Service.dart';
import 'package:http/http.dart' as http;

class Crud {
  String url = "https://jsonplaceholder.typicode.com/todos";

  Future getData() async {
    final response = await http.get(Uri.parse(url));
    var data = await jsonDecode(response.body);

    List<Service> listOfServices = [];

    for (var elem in data) {
      Service serv = Service(
          id: elem["id"].toString(),
          uid: elem["userId"].toString(),
          title: elem["title"],
          completed: elem["completed"].toString());
      listOfServices.add(serv);
    }
    return listOfServices;
  }

  Future createServices( int userId, String title, String completed) async {
    try {
      final response = await http.post(Uri.parse(url),
          body: jsonEncode(
              {"userId":userId , "title": title, "completed": completed}));
      if (response.statusCode == 201) {
        return true;
      } else {
        return false;
      }
    } on Exception catch (e) {
      e.toString();
    }
  }

  Future updateServices(
      String id, String userId, String title, String completed) async {
    try {
      final response = await http.put(Uri.parse(url),
          body: jsonEncode({
            "id": int.parse(id),
            "userId": int.parse(userId),
            "title": title,
            "completed": completed
          }));
      if (response.statusCode == 404) {
        return true;
      } else {
        return false;
      }
    } on Exception catch (e) {
      e.toString();
    }
  }

  Future deleteServices(int id) async {
    try {
      final response =
          await http.delete(Uri.parse(url), body: jsonEncode({"id": id}));
      if (response.statusCode == 404) {
        return true;
      } else {
        return false;
      }
    } on Exception catch (e) {
      e.toString();
    }
  }
}
