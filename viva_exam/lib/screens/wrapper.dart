import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:viva_exam/models/user.dart';
import 'package:viva_exam/screens/authenticate/authenticate.dart';
import 'package:viva_exam/screens/home/home.dart';

class Wrapper extends StatelessWidget {
  const Wrapper({super.key});

  @override
  Widget build(BuildContext context) {
    
    //either return home or authenticate screen
    dynamic user = Provider.of<CustomUser?>(context);

    if(user == null)
    {
      return Authenticate();
    }
    else{
      return Home();
    }

  }
}