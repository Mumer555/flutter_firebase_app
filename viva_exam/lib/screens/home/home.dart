import 'package:flutter/material.dart';
import 'package:viva_exam/services/auth.dart';

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    final AuthService _auth = AuthService();

    return Scaffold(
      backgroundColor: Colors.brown[100],
      appBar: AppBar(
        title: Text("Viva Exam"),
        backgroundColor: Colors.brown[400],
        actions: <Widget>[
          TextButton.icon(
            onPressed: () async {
              await _auth.signOut();
            },
            icon: Icon(Icons.person),
            label: Text(
              "Log Out",
            ),
          )
        ],
      ),
      body: const Center(
        child: Text('My Page!'),
      ),
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.brown,
              ),
              child: Text('Select Task'),
            ),
            ListTile(
              title: const Text(""),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            SizedBox(
              child: ElevatedButton(
                onPressed: (){
                  Navigator.pushNamed(context, "/homeee");
                },
                child: Text("Firebase Cruds"),
              ),
            ),
            ListTile(
              title: const Text(''),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            SizedBox(
              child: ElevatedButton(
                onPressed: (){
                  Navigator.pushNamed(context, "/ApiHome");
                },
                child: Text("Api Cruds"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
