import 'package:flutter/material.dart';
import 'package:viva_exam/screens/authenticate/sign_in.dart';
import 'package:viva_exam/screens/authenticate/sign_up.dart';

class Authenticate extends StatefulWidget {
  const Authenticate({super.key});

  @override
  State<Authenticate> createState() => _AuthenticateState();
}

class _AuthenticateState extends State<Authenticate> {
  
  bool showSignIn = true;
  void switchScreen() {
    setState(() {
      showSignIn = !showSignIn;
    });
  }
  
  @override
  Widget build(BuildContext context) {
    if(showSignIn){
      return SignIn(switchScreen: switchScreen);
    }
    else{
      return Register(switchScreen: switchScreen);
    }
  }
}