import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';
import 'package:viva_exam/FirebaseCrud/homePage.dart';
import 'package:viva_exam/models/user.dart';
import 'package:viva_exam/screens/wrapper.dart';
import 'package:viva_exam/services/auth.dart';
import 'package:viva_exam/FirebaseCrud/add.dart';
import 'package:viva_exam/FirebaseCrud/update.dart';

import 'package:viva_exam/API/homePage.dart';
import 'package:viva_exam/API/updatePage.dart';
import 'package:viva_exam/API/createPage.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: const FirebaseOptions(
          appId: '1:286647054580:android:5f4d0a89f82e6421e5eea1',
          apiKey: 'AIzaSyDMK3EpqAuvnF62LsyQiqiQ2jUl-ER6sdQ',
          messagingSenderId: '286647054580',
          projectId: 'viva-exam'));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return StreamProvider<CustomUser?>.value(
      initialData: null,
      value: AuthService().user,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Viva Exam',
        initialRoute: '/',
        routes: {
          "/": (context) => Wrapper(),
          "/homeee": (context) => FirebaseHome(),
          "/add": (context) => CreatePost(),
          "/update": (context) => Update(),
          "/ApiHome" : (context) => ApiHome(),
          "/Apicreate" : (context) => ApiCreatePage(),
          "/Apiupdate" : (context) => ApiUpdatePage(),  
        },
      ),
    );
  }
}
