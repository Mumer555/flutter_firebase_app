import 'package:firebase_auth/firebase_auth.dart';
import 'package:viva_exam/models/user.dart';

class AuthService{

  final FirebaseAuth _auth = FirebaseAuth.instance;
  
  // convert firebase User to a custom User
  CustomUser? _firebaseUserConversion(User? user) {
    return user != null ? CustomUser(uid: user.uid) : null;
  }

  // keeping track of auth changes
  Stream<CustomUser?> get user{
      return _auth.authStateChanges().map(
        _firebaseUserConversion
      );
  }
  
  // sign in anonymously
  Future signInAnon() async {
    try{
      UserCredential result = await _auth.signInAnonymously();
      User? user = result.user;
      return _firebaseUserConversion(user!);
    }catch(e){
      print(e.toString());
      return null;
    }
  }

  // Sign up with email and password
  Future registerWithEmailAndPassword(String email, String password) async {
    try{
      UserCredential result = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      User? user = result.user;
      return _firebaseUserConversion(user);
    }catch(e){
      print(e.toString());
      return null;
    }
  }

  // Sign in with email and password
  Future loginWithEmailAndPassword(String email, String password) async {
    try{
      UserCredential result = await _auth.signInWithEmailAndPassword(email: email, password: password);
      User? user = result.user;
      return _firebaseUserConversion(user);
    }catch(e){
      print(e.toString());
      return null;
    }
  }

  // Sign Out
  Future signOut() async {
    try{
      return await _auth.signOut();
    }catch(e){
      print(e.toString());
      return null;
    }
  }
}