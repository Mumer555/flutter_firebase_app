class Person{
  String id;
  String name;
  String gender;
  String email;
  String contact;

  Person({required this.id, required this.name, required this.gender, required this.email, required this.contact});
}