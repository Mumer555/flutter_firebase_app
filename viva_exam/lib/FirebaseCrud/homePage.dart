import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:viva_exam/FirebaseCrud/logic.dart';
import 'package:viva_exam/FirebaseCrud/person.dart';

class FirebaseHome extends StatefulWidget {
  const FirebaseHome({super.key});

  @override
  State<FirebaseHome> createState() => _FirebaseHomeState();
}

class _FirebaseHomeState extends State<FirebaseHome> {
  

  List<Person> listOfPersons = [];
  Repository repo = Repository();
  //*** To Display the data from firebase in the form of cards  */
  Widget getCardTemplate(elem) {
    return Card(
      child: Container(
        margin: EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Row(
              children: [
                Text("Name: ", style: TextStyle(fontWeight: FontWeight.bold)),
                Text("${elem.name}"),
              ],
            ),
            SizedBox(height: 3.0),
            Row(
              children: [
                Text("Gender: ", style: TextStyle(fontWeight: FontWeight.bold)),
                Text("${elem.gender}"),
              ],
            ),
            SizedBox(height: 3.0),
            Row(
              children: [
                Text("Email: ", style: TextStyle(fontWeight: FontWeight.bold)),
                Text("${elem.email}"),
              ],
            ),
            SizedBox(height: 3.0),
            Row(
              children: [
                Text("Contact: ", style: TextStyle(fontWeight: FontWeight.bold)),
                Text("${elem.contact}"),
              ],
            ),
            SizedBox(height: 8.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ElevatedButton.icon(
                    onPressed: () {
                      Navigator.pushNamed(context, "/update" , arguments: [elem.id,elem.name,elem.gender,elem.email,elem.contact]);
                    },
                    icon: Icon(Icons.update),
                    label: Text("update")),
                ElevatedButton.icon(
                    onPressed: () {
                      repo.deletePErson(elem.id);
                      Navigator.pushNamed(context, "/homeee");
                    },
                    icon: Icon(Icons.delete),
                    label: Text("delete")),
              ],
            ),
          ],
        ),
      ),
    );
  }

  // GetData from firebase
  getData() async {
    listOfPersons = await repo.getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Firebase Crud"),
        centerTitle: true,
        backgroundColor: Colors.orange,
      ),
      body: FutureBuilder(
        future: getData(),
        builder: (context , snapshot) {
          if(snapshot.connectionState == ConnectionState.done)
          {
            return Scrollbar(
              child: ListView.builder(
                itemCount: 1,
                itemBuilder: (context, index){
                  return Container(
                    padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
                    color: Colors.green[100],
                    child: Column(
                      children: listOfPersons.map((elem) => getCardTemplate(elem)).toList()
                    ),
                  );
                },
              ),
            );
          }
          if(snapshot.hasError)
          {
            return Text("error in loading data");
          }
          else{
            return CircularProgressIndicator();
          }
        },

      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Navigator.pushNamed(context, "/add");
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
