import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:viva_exam/FirebaseCrud/logic.dart';

class Update extends StatelessWidget {
  const Update({super.key});

  @override
  Widget build(BuildContext context) {
    Repository repo = Repository();
    final nameController = TextEditingController();
    final genderController = TextEditingController();
    final emailController = TextEditingController();
    final contactController = TextEditingController();

    var data = ModalRoute.of(context)?.settings.arguments as List<dynamic>;
    nameController.text = data[1];
    genderController.text = data[2];
    emailController.text = data[3];
    contactController.text = data[4];

    return Scaffold(
      appBar: AppBar(
        title: Text("Add Person"),
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            TextField(
              controller: nameController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Enter Name',
              ),
            ),
            SizedBox(height: 8.0),
            TextField(
              controller: genderController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Enter Gender',
              ),
            ),
            SizedBox(height: 8.0),
            TextField(
              controller: emailController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Enter Email',
              ),
            ),
            SizedBox(height: 8.0),
            TextField(
              controller: contactController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Enter Contact',
              ),
            ),
            SizedBox(height: 8.0),
            ElevatedButton(
                onPressed: () {
                  repo.updatePerson(data[0], nameController.text, genderController.text, contactController.text, emailController.text);
                  Navigator.pushNamed(context, "/homeee");
                },
                child: Text("Update")),
          ],
        ),
      ),
    );
  }
}
