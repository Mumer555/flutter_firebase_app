import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:viva_exam/FirebaseCrud/person.dart';

class Repository {
  Future getData() async {
    List<Person> persons = [];
    Person p;
    dynamic data = await FirebaseFirestore.instance
        .collection("Umer")
        .get()
        .then((snapshot) => {
              for (var elem in snapshot.docs)
                {
                  p = Person(
                      id: elem.id,
                      name: elem.data()["name"],
                      gender: elem.data()["gender"],
                      email: elem.data()["email"],
                      contact: elem.data()["contact"]),
                  persons.add(p)
                }
            });
    return persons;
  }

  createPerson(String name , String gender , String email , String contact) {
    final db = FirebaseFirestore.instance.collection("Umer");

    Map<String , String> data = {
      "name": name,
      "gender" : gender,
      "email" : email,
      "contact" : contact
    };

    db.add(data);
  }

  updatePerson(String id , String name , String gender , String contact , String email) {
    final db = FirebaseFirestore.instance.collection("Umer").doc(id);
    Map<String , String> data = {
      "name": name,
      "gender" : gender,
      "email" : email,
      "contact" : contact,
    };
    db.update(data);
  }

  deletePErson(String id) {
    FirebaseFirestore.instance.collection("Umer").doc(id).delete();
  }
}
